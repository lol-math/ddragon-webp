import gulp from "gulp";
import download from "gulp-download2";
import Ddragon from "ddragon";
import fetch from "node-fetch";
import decompress from "gulp-decompress";
import fs from "fs";
import count from "gulp-count";
import imagemin from "gulp-imagemin";
import imageminWebp from "imagemin-webp";
import rename from "gulp-rename";

let dd = new Ddragon();

let version = "";

function downloadDdVersion(done) {
  fetch(dd.versions())
    .then((res) => res.json())
    .then((versions) => {
      version = versions[0];
      console.log("ddragon version", version);
      done();
    });
}

function downloadDragonTail() {
  return download(
    `https://ddragon.leagueoflegends.com/cdn/dragontail-${version}.tgz`,
  ).pipe(gulp.dest("./temp"));
}

function extractFiles() {
  const path = `./temp/dragontail-${version}.tgz`;
  return gulp.src(path).pipe(decompress()).pipe(gulp.dest("temp/extract"));
}

function moveVersioned(done) {
  fs.rename(`./temp/extract/${version}/`, "./temp/extract/latest/", done);
}

function convertFiles() {
  return gulp
    .src([
      "temp/extract/**/*.png",
      "temp/extract/**/*.jpg",
      "!temp/extract/**/sprite/*",
      "!temp/extract/**/tft-*/*",
      "!temp/extract/**/challenges-*/*",
      "!temp/extract/**/mission/*",
    ])
    .pipe(imagemin([imageminWebp()]))
    .pipe(rename({ extname: ".webp" }))
    .pipe(gulp.dest("./public/"))
    .pipe(count());
}

export default gulp.series(
  downloadDdVersion,
  downloadDragonTail,
  extractFiles,
  moveVersioned,
  convertFiles,
);
